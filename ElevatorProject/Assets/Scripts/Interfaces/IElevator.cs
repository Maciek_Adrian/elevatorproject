﻿using UnityEngine;

public interface IElevator 
{
    void Travel(Transform destination);
}
