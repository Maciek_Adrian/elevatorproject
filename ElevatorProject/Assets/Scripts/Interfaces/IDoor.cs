﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDoor 
{
    bool Open();

    bool Close();

}
