﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UseInteractables : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] [Range(0, 100)] private float maxDistance;

    private Iinteractable CurrInteractable;

    void Update()
    {
        RaycastSingle();
    }

    private void RaycastSingle()
    {
        Vector3 origin = transform.position;
        Vector3 direction = transform.forward;

        Ray ray = new Ray(origin, direction);
        RaycastHit raycastHit;

        if (Physics.Raycast(ray, out raycastHit, maxDistance, layerMask))
        {
            if (raycastHit.collider.GetComponent<Iinteractable>() == null) return;
            CurrInteractable = raycastHit.collider.GetComponent<Iinteractable>();

            CurrInteractable.HighLightObject();
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                CurrInteractable.Interact();
            }
        }
        else
        {
            if (CurrInteractable != null)
                CurrInteractable.UnHighLightObject();
        }

    }
}

