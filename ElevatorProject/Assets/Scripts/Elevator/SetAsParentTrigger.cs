﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class SetAsParentTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() == null) return;

        other.transform.parent = transform;
        other.transform.Rotate(new Vector3(0, -transform.rotation.eulerAngles.y, 0));

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() == null) return;

        other.transform.Rotate(new Vector3(0, transform.rotation.eulerAngles.y, 0));
        other.transform.parent = null;
    }
}
