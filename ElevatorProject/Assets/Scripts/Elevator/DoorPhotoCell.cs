﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPhotoCell : MonoBehaviour
{


    [SerializeField] private bool hasCollider = false;
    [SerializeField] private List<GameObject> colliders;

    public bool HasCollider()
    {
        return hasCollider;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() == null) return;

        colliders.Add(other.gameObject);
        hasCollider = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() == null) return;

        colliders.Remove(other.gameObject);
        if (colliders.Count == 0)
        {
            hasCollider = false;
        }
    }
}
