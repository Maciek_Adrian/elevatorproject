﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorBtn : MonoBehaviour , Iinteractable
{
    [SerializeField] private Light highLight;
    [SerializeField] private Transform Destination;
    [SerializeField] private Elevator Elevator;
    [SerializeField] private SimpleAudioEvent AudioEvent;
    [SerializeField] private AudioSource AudioSource;


    public void HighLightObject()
    {
        highLight.enabled = true;
    }

    public void UnHighLightObject()
    {
        highLight.enabled = false;
    }

    public void Interact()
    {
        AudioEvent.Play(AudioSource);
        Elevator.Travel(Destination);
    }

}
