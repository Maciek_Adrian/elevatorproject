﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour, IElevator
{
    [SerializeField] private Transform CurrentFloor;
    [SerializeField] private float Speed;
    [SerializeField] private float TimeOpenAtDestination = 5;
    [SerializeField] private AudioEvent MovingAudioEvent;
    [SerializeField] private AudioEvent StartAudioEvent;
    [SerializeField] private AudioEvent StopAudioEvent;

    private AudioSource audioSource;
    private IDoor door;
    private Transform Destination;
    private Vector3 startingPosition;
    private float travelledPercentage = 1;
    private bool moving;
    private bool engine_working = false;

    public void Travel(Transform destination)
    {
        StopAllCoroutines();
        ResetOngoiongTravel();
        moving = false;
        Destination = destination;
        StartCoroutine("TravelCoroutine");
    }

    private void Start()
    {
        door = GetComponentInChildren<IDoor>();
        audioSource = GetComponent<AudioSource>();
    }
    private void FixedUpdate()
    {
        CheckMovement();
    }

    private void ResetOngoiongTravel()
    {
        if (travelledPercentage >= 1) return;
        travelledPercentage = 0;
    }

    private void CheckMovement()
    {

        if (travelledPercentage < 1 && moving)
        {
            travelledPercentage += Time.fixedDeltaTime * (Speed / Vector3.Magnitude(Destination.position - startingPosition));
            transform.position = Vector3.Lerp(startingPosition, Destination.position, travelledPercentage);
        }
    }

    private IEnumerator TravelCoroutine()
    {
        SetStartingPosition(transform.position);
        float travelSqrMagnitude = (Destination.position - startingPosition).sqrMagnitude;
        if (CurrentFloor != Destination)
        {
            while (!CloseDoor()) { yield return null; }
            if (!engine_working)
            {
                engine_working = true;
                StartAudioEvent.Play(audioSource);
                while (AudioSourcePlaying()) { yield return new WaitForEndOfFrame(); }
            }
        }

        if (travelSqrMagnitude > 0)
        {
            MovingAudioEvent.Play(audioSource);
            moving = true;
            travelledPercentage = 0;
            while (!CheckIfDestinationReached()) { yield return new WaitForEndOfFrame(); }
            moving = false;
            CurrentFloor = Destination;
            engine_working = false;
            StopAudioEvent.Play(audioSource);
            while (AudioSourcePlaying()) { yield return new WaitForEndOfFrame(); }
        }
        OpenDoor();
        yield return new WaitForSeconds(TimeOpenAtDestination);
        while (!CloseDoor()) { yield return new WaitForEndOfFrame(); }
        yield return null;
    }

    private void SetStartingPosition(Vector3 position)
    {
        startingPosition = position;
    }

    private void OpenDoor()
    {
        door.Open();
        if (Destination.GetComponentInChildren<IDoor>() != null) Destination.GetComponentInChildren<IDoor>().Open();
    }

    private bool CloseDoor()
    {
        if (CurrentFloor.GetComponentInChildren<IDoor>() != null) CurrentFloor.GetComponentInChildren<IDoor>().Close();
        return door.Close();
    }

    private bool AudioSourcePlaying()
    {
        return audioSource.isPlaying;
    }

    private bool CheckIfDestinationReached()
    {
        return travelledPercentage >= 1;
    }

}
