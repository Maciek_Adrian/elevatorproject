﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IDoor
{
    [SerializeField] private AudioEvent audioEvent;

    private DoorPhotoCell doorPhotoCell;
    private Animator anim;
    private AudioSource audioSource;
    private AnimatorStateInfo animatorStateInfo;
    private bool soundPlaying = false;


    private void Update()
    {
        PlaySoundWhenAnimating();
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        doorPhotoCell = GetComponentInChildren<DoorPhotoCell>();
        audioSource = GetComponent<AudioSource>();
        anim.Play("CloseDoorAnimation", 0, 1);
    }

    public bool Close()
    {

        if (!doorPhotoCell.HasCollider() && CheckIfDoorOpened())
        {
            SwitchToClosingAnimation();
        }
        else if(doorPhotoCell.HasCollider())
        {
            SwitchToOpeningAnimation();
        }

        return CheckIfDoorClosed();
    }

    public bool Open()
    {
        SwitchToOpeningAnimation();
        return true;
    }

    private void SwitchToClosingAnimation()
    {
        if (animatorStateInfo.IsName("CloseDoorAnimation")) return;

        if (animatorStateInfo.normalizedTime < 1)
        {
            anim.Play("CloseDoorAnimation", 0, 1 - animatorStateInfo.normalizedTime);
        }
        else
        {
            anim.Play("CloseDoorAnimation", 0, 0);
        }
    }

    private void SwitchToOpeningAnimation()
    {
        if (animatorStateInfo.IsName("OpenDoorAnimation")) return;

        if (animatorStateInfo.normalizedTime < 1)
        {
            anim.Play("OpenDoorAnimation", 0, 1 - animatorStateInfo.normalizedTime);
        }
        else
        {
            anim.Play("OpenDoorAnimation", 0, 0);
        }
    }

    private bool CheckIfDoorClosed()
    {
        if (animatorStateInfo.normalizedTime > 1 && animatorStateInfo.IsName("CloseDoorAnimation") && !anim.IsInTransition(0))
        {
            return true;
        }
        return false;
    }

    private bool CheckIfDoorOpened()
    {
        if (animatorStateInfo.normalizedTime > 1 && animatorStateInfo.IsName("OpenDoorAnimation") && !anim.IsInTransition(0))
        {
            return true;
        }
        return false;
    }

    private void PlaySoundWhenAnimating()
    {
        animatorStateInfo = anim.GetCurrentAnimatorStateInfo(0);

        if (animatorStateInfo.IsName("DoorStartingPose")) return;


        if (soundPlaying && animatorStateInfo.normalizedTime >= 1)
        {
            soundPlaying = false;
            audioEvent.Stop(audioSource);
        }
        else if (!soundPlaying && animatorStateInfo.normalizedTime < 1)
        {
            soundPlaying = true;
            audioEvent.Play(audioSource);
        }
    }
}
